package com.puravida.blog.docurest;

import com.jayway.restassured.http.ContentType;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.restdocs.JUnitRestDocumentation;

import static org.springframework.restdocs.request.RequestDocumentation.*;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.*;
import static org.springframework.restdocs.restassured.RestAssuredRestDocumentation.*;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.hamcrest.CoreMatchers.*;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.specification.RequestSpecification;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.restdocs.request.ParameterDescriptor;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by jorge on 22/01/17.
 */
public class DocuTest {

    //# tag::snippets[]
    @Rule
    public JUnitRestDocumentation restDocumentation =
            new JUnitRestDocumentation("target/generated-snippets"); //<1>
    //# end::snippets[]

    private RequestSpecification spec;

    //# tag::beforeTest[]
    @Before
    public void beforeTest() throws Exception{
        spec = new RequestSpecBuilder()
                .setBaseUri("http://www.recipepuppy.com/api/")  //<1>
                .addFilter(documentationConfiguration(restDocumentation))
                .build();
    }
    //# end::beforeTest[]

    List<FieldDescriptor> getMainDescriptor(){
        List<FieldDescriptor> ret = new ArrayList<FieldDescriptor>();
        ret.add(fieldWithPath("title").description("El titulo de la receta").type(String.class));
        ret.add(fieldWithPath("version").description("Ni idea de a que se refiere").type(String.class).optional());
        ret.add(fieldWithPath("href").description("Un enlace a la pagina web ").type(String.class).optional());
        ret.add(fieldWithPath("results[].").description("Un array de resultados").type(Object.class));
        return ret;
    }

    //# tag::recipeDescriptor[]
    List<FieldDescriptor> getRecipeDescriptor(){    //<1>
        List<FieldDescriptor> ret = new ArrayList<FieldDescriptor>();
        ret.add(fieldWithPath("title").description("El titulo de la receta").type(String.class));
        ret.add(fieldWithPath("href").description("Un enlace a la pagina web ").type(String.class).optional());
        ret.add(fieldWithPath("ingredients").description("Un array de ingredientes separados por coma").type(String.class));
        return ret;
    }
    //# end::recipeDescriptor[]


    //# tag::recipeList[]
    @Test
    public void getRecipeList() throws Exception{   //<1>
        RestAssured.given(this.spec)
                .accept(ContentType.JSON)
                .filter(
                        document("recipe_list", //<2>
                                preprocessResponse(prettyPrint()),
                                responseFields(
                                        getMainDescriptor()
                                ).andWithPrefix("results[].",getRecipeDescriptor())     //<3>
                        )
                )
                .when().get("/")    // <4>
                .then().assertThat().statusCode(is(200));   // <5>
    }
    //# end::recipeList[]


    //# tag::recipeWithIngredient[]
    @Test
    public void getRecipeWithIngredient() throws Exception{
        RestAssured.given(this.spec)
                .accept(ContentType.JSON)
                .filter(
                        document("recipe_ingredient",
                                preprocessResponse(prettyPrint()),
                                responseFields(
                                        getMainDescriptor()
                                ).andWithPrefix("results[].",getRecipeDescriptor()),
                                requestParameters(
                                        parameterWithName("i").description("El ingrediente a buscar"), //<1>
                                        parameterWithName("p").description("Pagina a retornar")
                                )
                        )
                )
                .when().get("/?i=onions&p=3")   //<1>
                .then().assertThat().statusCode(is(200));
    }
    //# end::recipeWithIngredient[]

}
